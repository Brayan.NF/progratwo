package Inventory;

import java.util.Scanner;

public class Inventory {
    
    String[][] bag;

    public Inventory() {
        bag = new String[8][8];
        emptyBag();
    }

    public Inventory(int size) {
        bag = new String[size][size];
        emptyBag();
    }

    public Inventory(int x, int y) {
        bag = new String[x][y];
        emptyBag();
    }

    private void emptyBag() {
        for (int i = 0; i < bag.length; i++) {
            for (int j = 0; j < bag[0].length; j++) {
                bag[i][j] = "-";
            }
        }
    }

    public void showBag() {

        for (int i = 0; i < bag.length; i++) {
            for (int j = 0; j < bag[0].length; j++) {
                System.out.print(bag[i][j]);
            }
            System.out.println();
        }
    }

    public void addItem(Item item) {

        String option = " ";
        int x = 0;
        int y = 0;
        Scanner scan = new Scanner(System.in);
        while (!option.equals("ok")) {
            showPreview(x, y, item);
            System.out.println("Move your item (w,a,s,d), press 'ok' to confirm");
            option = scan.nextLine();
            switch (option) {
                case "w":
                    x--;
                    break;
                case "d":
                    y++;
                    break;
                case "a":
                    y--;
                    break;
                case "s":
                    x++;
                    break;
                case "ok":
                    placeItem(x, y, item);
                    break;
                default:
                    System.out.println("Wrong option, try again");
            }
        }
    }

    private void placeItem(int x, int y, Item item) {

        for (int i = 0; i < item.getLenght(); i++) {
            for (int j = 0; j < item.getWidth(); j++) {
                bag[x + i][y + j] = item.getId();
            }
        }
        System.out.println("placed");
    }

    private void showPreview(int x, int y, Item item) {

        String string = (checkSpace(x, y, item.getLenght(), item.getWidth()))?"O":"X";
        for (int i = 0; i < bag.length; i++) {
            for (int j = 0; j < bag[0].length; j++) {
                if (i >= x && i < x + item.getLenght() && j >= y && j < y + item.getWidth()) {
                    System.out.print(string);
                } else {
                    System.out.print(bag[i][j]);
                }
            }
            System.out.println();
        }
    }

    private boolean checkSpace(int x, int y, int lenght, int width) {

        for (int i = 0; i < lenght; i++) {
            for (int j = 0; j < width; j++) {
                if (bag[x + i][y + j] != "-") {
                    return false;
                }
            }
        }
        return true;
    }
}
