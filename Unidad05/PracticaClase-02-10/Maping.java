import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Maping {
    Map<String, Integer> map;

    public Maping() {
        map = new TreeMap<>();
    }

    public void readText() {
        System.out.println("insert text");
        Scanner scan = new Scanner(System.in);
        String text = scan.nextLine();
        String[] words = text.split(" ");
        fillMap(words);
    }

    private void fillMap(String[] words) {

        for (String word : words) {
            if (map.containsKey(word.toLowerCase())) {
                int newValue = map.get(word) + 1;
                map.replace(word, newValue);
            } else {
                map.put(word.toLowerCase(), 1);
            }
        }
    }

    public void showResult() {
        for (Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
    }
}
