public class Vigenere {
    
    private String[] code = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P",
                                "Q","R","S","T","U","V","W","X","Y","Z"};
    
    public String codify(String message, String key) {

        int index = 0;
        int combinedIndex = 0;
        String codedMessage = "";
        for (int i = 0; i < message.length(); i++) {
            if (index == key.length()) {
                index = 0;
            }
            if (message.charAt(i) == 32) {
                i++;
                codedMessage += " ";
            }
            combinedIndex = indexOfCode(message.charAt(i)) + indexOfCode(key.charAt(index));
            if (combinedIndex > 27) {
                combinedIndex = combinedIndex - 27;
            }
            codedMessage += code[combinedIndex];
            index++;
        }
        return codedMessage;
    }

    private int indexOfCode(char letter) {

        for (int i = 0; i < code.length; i++) {
            if (String.valueOf(letter).equals(code[i])) {
                return i;
            }
        }
        return 0;
    }

    public String decodify(String codified, String key) {

        int index = 0;
        String message = "";
        for (int i = 0; i < codified.length(); i++) {
            if (index == key.length()) {
                index = 0;
            }
            if (codified.charAt(i) == 32) {
                i++;
                message += " ";
            }
            int counter = 0;
            for (int j = indexOfCode(key.charAt(index)); j < code.length; j++) {
                if (code[j].equals(String.valueOf(codified.charAt(i)))) {
                    message += code[counter];
                    break;
                } else {
                    if (j == 26) {
                        j = -1;
                    }
                }
                counter++;
            }
            index++;
        }
        return message;
    }
}