/**
 * Exe
 */
public class Exe {

    public static void main(String[] args) {
        Vigenere vigenere = new Vigenere();
        System.out.println("Ejercicio 1");
        System.out.println("Entrada: ");
        String entry = System.console().readLine();
        System.out.println("Clave: ");
        String pass = System.console().readLine();
        System.out.println("1. Codificar\n2.Decodificar");
        int key = Integer.parseInt(System.console().readLine());
        if (key == 1) {
            System.out.println(vigenere.codify(entry, pass));
        } else {
            System.out.println(vigenere.decodify(entry, pass));
        }
    }
}