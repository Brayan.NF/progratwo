import java.util.Random;

public class Player {
    
    private String playerId;
    private String playerName;
    private int[] playerScore = new int[12];

    public Player(String id, String name) {
        playerId = id;
        playerName = name;
    }

    public void playGame() {

        /*
        No encontré el sistema de puntuación del juego, por lo que no tiene sentido
        dar puntajes a cada una de las secciones, al final solo importa el score de
        cada nivel y es lo que estoy generando, un score final para cada nivel.
        */
        Random rdm = new Random();
        for (int i = 0; i < playerScore.length; i++) {
            playerScore[i] = rdm.nextInt(101);
        }
    }

    public int getTotalScore() {

        int prom = 0;
        for (int score : playerScore) {
            prom += score;
        }
        return prom / 13;
    }

    public int getLevelScore(int level) {
        return playerScore[level];
    }
}