package Practica3.Parte3;

public class Board {
    
    private boolean turn = true;
    private char[][] board = new char[3][3];

    /**
     * Constructor, fill the board with "-".
     */
    public Board() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = 45;
            }
        }
    }

    /**
     * Show board values
     */
    public void showBoard() {

        System.out.println();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(" " + String.valueOf(board[i][j]));
            }
            System.out.println();
        }
    }

    /**
     * Check if row and column parameters are in range.
     * @param row row parameter value
     * @param col column parameter value
     * @return true if parameters are in range
     */
    private boolean checkParameters(int row, int col) {

        if (row >= 0 && row < board.length) {
            if (col >= 0 && row < board.length) {
                return true;
            }
        }
        return false;
    }

    /**
     * Mark the movement on the board, also check if the space is empty.
     * @param row input row parameter
     * @param col input column parameter
     * @return true if it's possible to mark
     *          false if ther isn't an empty space
     */
    public boolean mark(int row, int col) {

        if (!checkParameters(row, col)) {
            return false;
        }
        if (board[row][col] == 45) {
            char mark = (turn)?(char)88:79;
            board[row][col] = mark;
            turn = changeTurn();
            return true;
        }
        return false;        
    }

    /**
     * change the state of player's turn variable.
     * @return true if "turn" is false
     *          false if "turn" is true
     */
    private boolean changeTurn() {
        return (!turn);
    }

    /**
     * Check if some player won.
     * take the char value of each line and adds it.
     * X = 88 * 3 = 264
     * O = 79 * 3 = 237
     * @return true if someone won
     */
    public boolean checkWin() {

        int row = 0;
        int col = 0;
        int diagonal1 = 0;
        int diagonal2 = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                row += board[i][j];    
                col += board[j][i];
            }
            if (row == 264 || row == 237 || col == 264 || col == 237) {
                return true;
            }
            row = col = 0;
            diagonal1 += board[i][i];
            diagonal2 += board[i][2 - i];
        }
        if (diagonal1 == 264 || diagonal1 == 237 || diagonal2 == 264 || diagonal2 == 237) {
            return true;
        }
        return false;
    }
}