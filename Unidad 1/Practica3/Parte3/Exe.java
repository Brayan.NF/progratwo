package Practica3.Parte3;

import java.util.Scanner;

public class Exe {
    
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        int movements = 1;
        Board board = new Board();
        while (movements < 10) {
            board.showBoard();
            if (board.checkWin()) {
                System.out.println("Ganaste!!");
                break;
            }
            System.out.println("Row (1-3)");
            int row = Integer.parseInt(scan.nextLine()) - 1;
            System.out.println("Column (1-3)");
            int col = Integer.parseInt(scan.nextLine()) - 1;
            if (board.mark(row, col)) {
                movements++;
            } else {
                System.out.println("Error, try again");
            }
        }
        System.out.println("\nFin del juego");
    }
}