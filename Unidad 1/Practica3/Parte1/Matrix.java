package Practica3.Parte1;

import java.util.Scanner;

public class Matrix {
    
    private int[][] matrix = new int[4][4];
    private boolean isfull = false;
    Scanner scan = new Scanner(System.in);

    /**
     * check if matrix is full, if it's not, fill it.
     */
    public void fillMatrix() {

        if (isfull) {
            System.out.println("Already done!");
        } else {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    System.out.println("Ingrese valor de la fila " + (i + 1) + ", columna " + (j + 1));
                    matrix[i][j] = Integer.parseInt(scan.nextLine());
                }
            }
            isfull = true;
        }        
    }

    /**
     * adds the row indicated by the user, checking if the row is correct.
     */
    public void sumRow() {

        if (isfull) {
            System.out.println("Ingrese fila a sumar (1 - 4)");
            int row = Integer.parseInt(scan.nextLine()) - 1;
            int result = 0;
            if (row >= 0 && row < matrix.length) {
                for (int i = 0; i < matrix.length; i++) {
                    result += matrix[row][i];
                }
                System.out.println("Resultado: " + result);
            } else {
                System.out.println("Error, ingrese una fila entre 1 y 4");
            }                    
        } else {
            System.out.println("Debe llenar la matriz primero, elija la opción 1");
        }
    }

    /**
     * adds the column indicated by the user, checking if the column is correct.
     */
    public void sumColumn() {

        if (isfull) {
            System.out.println("Ingrese columna a sumar (1 - 4)");
            int col = Integer.parseInt(scan.nextLine()) - 1;
            int result = 0;
            if (col >= 0 && col < matrix.length) {
                for (int i = 0; i < matrix.length; i++) {
                    result += matrix[i][col];
                }
                System.out.println("Resultado: " + result);
            } else {
                System.out.println("Error, ingrese una columna entre 1 y 4");
            }                    
        } else {
            System.out.println("Debe llenar la matriz primero, elija la opción 1");
        }
    }

    /**
     * adds the main diagonal
     */
    public void addMainDiagonal() {

        if (isfull) {
            int result = 0;
            for (int i = 0; i < matrix.length; i++) {
                result += matrix[i][i];
            }                    
            System.out.println("Resultado: " + result);
        } else {
            System.out.println("Debe llenar la matriz primero, elija la opción 1");
        }
    }

    /**
     * adds the reverse diagonal
     */
    public void addReverseDiagonal() {

        if (isfull) {
            int result = 0;
            for (int i = 0; i < matrix.length; i++) {
                result += matrix[i][3 - i];
            }                    
            System.out.println("Resultado: " + result);
        } else {
            System.out.println("Debe llenar la matriz primero, elija la opción 1");
        }
    }

    /**
     * Show the average of all values.
     */
    public void average() {

        if (isfull) {
            int sum = 0;
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    sum += matrix[i][j];                    
                }
            }
            System.out.println("Resultado: " + (sum / 16));
        } else {
            System.out.println("Debe llenar la matriz primero, elija la opción 1");
        }
    }
}