package Practica3.Parte1;

import java.util.Scanner;

/**
 * Exe
 */
public class Exe {

    public static void main(String[] args) {
        
        int option = 0;
        Matrix matrix = new Matrix();
        while (option != 7) {
            System.out.println("Menu");
            System.out.println("1. Rellenar matriz");
            System.out.println("2. Sumar fila");
            System.out.println("3. Sumar columna");
            System.out.println("4. Sumar diagonal principal");
            System.out.println("5. Sumar diagonal inversa");
            System.out.println("6. Media de todos los valores");
            System.out.println("7. Salir");
            Scanner scan = new Scanner(System.in);
            option = Integer.parseInt(scan.nextLine());
            switch (option) {
                case 1:
                    matrix.fillMatrix();
                    break;
                case 2:
                    matrix.sumRow();
                    break;
                case 3:
                    matrix.sumColumn();
                    break;
                case 4:
                    matrix.addMainDiagonal();
                    break;
                case 5:
                    matrix.addReverseDiagonal();
                    break;
                case 6:
                    matrix.average();
                    break;        
                default:
                    System.out.println("Invalid option, try again");
            }
        }
    }
}