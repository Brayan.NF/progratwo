public class Person {
    
    private String name;
    private int age;
    public String dni;
    private char gender;
    private double weight;
    private double height;

    public Person() {
        
        name = "";
        age = 0;
        gender = 'H';
        weight = 0;
        height = 0;
        generateDNI();
    }

    public Person(String name, int age, char gender) {
        
        this.name = name;
        this.age = age;
        this.gender = gender;
        weight = 0;
        height = 0;
        generateDNI();
        genderCheck();
    }

    public Person(String name, int age, char gender, double weight, double height) {
        
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.weight = weight;
        this.height = height;
        generateDNI();
        genderCheck();
    }

    public int calculateIMC() {

        double result = weight / Math.pow(height, 2);
        if (result < 20) {
            return -1;
        } else if (result > 25) {
            return 1;
        }
        return 0;
    }

    public boolean isOlder() {
        
        if (age >= 18) {
            return true;
        }
        return false;
    }

    private void genderCheck() {

        if (gender != 'H') {
            if (gender != 'M') {
                gender = 'H';
            }
        }
    }

    public String toString() {
        String genderString = (gender == 'H')?"male":"female";
        return "Name: " + name + ", Age: " + age + ", DNI: " + dni + ", Gender: " + genderString
            + ", Weight: " + weight + ", Height: " + height;
    }

    private void generateDNI() {

        String newDNI = "";
        int number = 0;
        for (int i = 0; i < 8; i++) {
            number = (int) (Math.random() * 10);
            newDNI = newDNI + number;
        }
        dni = newDNI + "-" + (char) (65 + number);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}