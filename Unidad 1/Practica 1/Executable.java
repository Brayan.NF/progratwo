import java.util.Scanner;

public class Executable {
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Name:");
        String name = scan.nextLine();
        System.out.println("Age:");
        int age = Integer.parseInt(scan.nextLine());
        System.out.println("Gender (H/M):");
        char gender = scan.nextLine().charAt(0);
        System.out.println("Weight:");
        double weight = Double.parseDouble(scan.nextLine());
        System.out.println("Height:");
        double height = Double.parseDouble(scan.nextLine());

        Person person1 = new Person(name, age, gender, weight, height);
        Person person2 = new Person(name, age, gender);
        Person person3 = new Person();
        person3 = setPerson3(person3);

        checkStatus(person1, person2, person3);
    }

    private static Person setPerson3(Person person3) {

        person3.setName("Brayan");
        person3.setAge(29);
        person3.setGender('H');
        person3.setHeight(1.64);
        person3.setWeight(87);
        return person3;
    }

    private static void checkStatus(Person p1, Person p2, Person p3) {

        System.out.println("El objeto 1 está " + statusMessage(p1.calculateIMC()) + ", " + checkAge(p1));
        System.out.println(p1.toString() + "\n");
        System.out.println("El objeto 2 está " + statusMessage(p2.calculateIMC()) + ", " + checkAge(p2));
        System.out.println(p2.toString() + "\n");
        System.out.println("El objeto 3 está " + statusMessage(p3.calculateIMC()) + ", " + checkAge(p3));
        System.out.println(p3.toString());
    }

    private static String statusMessage(int result) {

        switch (result) {
            case 0:
                return "por debajo de su peso ideal";
            case 1:
                return "con sobrepeso";
            default:
                return "en su peso ideal";
        }
    }

    private static String checkAge(Person person) {
        if (person.isOlder()) {
            return "Es mayor de edad";
        }
        return "No es mayor de edad";
    }
}