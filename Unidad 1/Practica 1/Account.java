/**
 * Account
 */
public class Account {

    private String headLine;
    private double quantity;

    public Account(String headLine) {
        this.headLine = headLine;
    }

    public Account(String headLine, double quantity) {
        this.headLine = headLine;
        this.quantity = quantity;
    }

    public String getHeadLine() {
        return headLine;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setHeadLine(String newHeadLine) {
        this.headLine = newHeadLine;
    }

    public void setQuantity(double newQuantity) {
        this.quantity = newQuantity;
    }

    public String headLineToString() {
        return headLine;
    }

    public String quantityToString() {
        return "" + quantity;
    }

    public void enter(double quantity) {
        if (quantity >= 0) {
            this.quantity = quantity;
        }
    }

    public void remove(double quantity) {
        this.quantity -= quantity;
        if (this.quantity < 0) {
            this.quantity = 0;
        }
    }
}