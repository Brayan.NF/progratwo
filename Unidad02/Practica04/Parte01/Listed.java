package Unidad02.Practica04.Parte01;

import java.util.ArrayList;
import java.util.Scanner;

public class Listed {

    private ArrayList<Integer> numbers = new ArrayList<>();
    private ArrayList<Integer> moreThanMed = new ArrayList<>();
    private Scanner scan = new Scanner(System.in);
    private int sum = 0;
    private int media = 0;

    public void askCalculateAndShow() {

        int number = 0;
        while (number != -99) {
            System.out.println("Ingrese un numero, o -99 para terminar");
            number = Integer.parseInt(scan.nextLine());
            addNumber(number);
        }
        calculate();
        showResults();
    }

    private void addNumber(int number) {
        
        if (number != -99) {
            numbers.add(number);
        }
    }

    private void calculate() {

        for (Integer num : numbers) {
            sum += num;
        }
        media = sum / numbers.size();
        for (Integer num : numbers) {
            if (num > media) {
                moreThanMed.add(num);
            }
        }
    }

    public void showResults() {
        
        System.out.println("total numeros: " + numbers.size());        
        System.out.println("Suma: " + sum);
        System.out.println("Media: " + media);
        System.out.println("total números mayores a la media: " + moreThanMed.size());
        System.out.println("los mayores son:");
        for (Integer integer : moreThanMed) {
            System.out.print(integer + " ");
        }
    }
}