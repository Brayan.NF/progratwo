package Unidad02.Practica04.Parte02;

import java.util.ArrayList;
import java.util.Scanner;

public class Football {
    
    ArrayList<Team> teams = new ArrayList<>();
    Scanner scan = new Scanner(System.in);
    ArrayList<Ficture> ficture = new ArrayList<>();

    public void fillTeams() {

        String text = "";
        while (!text.equals("fin")) {
            System.out.println("Nombre de equipo, 'fin' para terminar");
            text = scan.nextLine();
            addTeam(text);
        }        
    }

    private void addTeam(String text) {

        if (!text.equals("fin")) {
            Team team = new Team(text);
            teams.add(team);
        }
    }

    public void matchMaking() {
        Ficture fic = new Ficture();
        ficture = fic.makeMatches(teams);
    }

    public void showFicture() {

        for (Ficture fic : ficture) {
            fic.showValues();
        }
    }

    public void playGames() {
        
        int total = 99999;
        while (total > ficture.size()) {
            System.out.println("Cuántos partidos simulamos?");
            total = Integer.parseInt(scan.nextLine());
        }
        for (int i = 0; i < total; i++) {
            ficture.get(i).figth();
        }
    }
    
    public void showTable() {

        System.out.println("TABLA DE POSICIONES");
        System.out.println("Equipo              Played  Won  Tie  Points");
        for (Team team : teams) {
            String space = "";
            int spaceNum = 20 - team.getName().length();
            for (int i = 0; i < spaceNum; i++) {
                space += " ";
            }
            System.out.println(team.getName() + space + team.getPlayed() + "       " 
                                + team.getWon() + "    " + team.getTied() + "    " + team.getPoints());
        }
    }
}