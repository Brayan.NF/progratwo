package Unidad02.Practica04.Parte02;

import java.util.ArrayList;

public class Ficture {
    
    private Team local;
    private Team visitant;
    private String date;

    public Ficture() {
    }

    public Ficture(Team local, Team visitant) {

        this.local = local;
        this.visitant = visitant;
    }

    /**
     * Fill the ficture list, save a local and visitant team.
     * @param teams list of teams.
     * @return list of all matches.
     */
    public ArrayList<Ficture> makeMatches(ArrayList<Team> teams) {

        ArrayList<Ficture> matches = new ArrayList<>();
        for (int i = 0; i < teams.size(); i++) {
            for (int j = 0; j < teams.size(); j++) {
                if (teams.get(i).getName() != teams.get(j).getName()) {
                    Ficture ficture = new Ficture(teams.get(i), teams.get(j));
                    matches.add(ficture);
                }
            }
        }
        return matches;
    }

    /**
     * Print the current match.
     * @param input current match.
     */
    public void showValues() {
        System.out.println(local.getName() + " vs. " + visitant.getName());
    }

    public void figth() {

        int localGoals = (int) Math.random() * 10;
        int visitGoals = (int) Math.random() * 10;
        if (localGoals > visitGoals) {
            local.setWin();
        } else if (localGoals < visitGoals) {
            visitant.setWin();
        } else {
            local.setTie();
            visitant.setTie();
        }
        local.setPlayed();
        visitant.setPlayed();
    }
}