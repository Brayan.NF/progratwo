package Unidad02.Practica04.Parte02;

public class Team {
    
    private String name;
    private int played = 0;
    private int won = 0;
    private int tied = 0;

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getWon() {
        return won;
    }

    public int getPlayed() {
        return played;
    }

    public int getTied() {
        return tied;
    }

    public int getPoints() {
        return (won * 3) + tied;
    }
    
    public void setPlayed() {
        played++;
    }

    public void setWin() {
        won++;
    }

    public void setTie() {
        tied++;
    }
}